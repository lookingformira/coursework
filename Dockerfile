FROM openjdk:8
ADD /target/scala-2.13/coursework-assembly-0.1.jar crawler.jar
ENTRYPOINT ["java", "-jar", "crawler.jar"]