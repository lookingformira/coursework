package coursework.parser

import coursework.client.ClientImpl
import coursework.data.{FilmFromIvi, Price, Purchase, Subscription}
import coursework.exceptions.FilmNotFoundException
import org.scalatest.flatspec.AsyncFlatSpec

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class ParserTest extends AsyncFlatSpec {

  val client = new ClientImpl

  val parser = new ParserImpl

  val title = "человек"

  behavior of "getFilm"

  it should "get films" in {

    val films: Future[Seq[FilmFromIvi]] = for {
      filmUrl <- client.filmUrl(title, 1)
      page <- client.getContent(filmUrl)
      films <- parser.getFilm(page)
    } yield films

    films map { seq => assert(seq.nonEmpty) }

  }

  it should "not get films" in {

    assertThrows[FilmNotFoundException] {

      val res = parser.getFilm("fault")

      Await.result(res, Duration.Inf)
    }
  }

  behavior of "getPrice"

  val idPayFilm = 457609
  val idSubFilm = 128103

  it should "get price" in {

    val films: Future[Price] = for {
      subUrl <- client.subscriptionUrl
      subPage <- client.getContent(subUrl)
      subPrice <- parser.getSubPrice(subPage)
      priceUrl <- client.priceUrl(idPayFilm)
      page <- client.getContent(priceUrl)
      price <- parser.getPrice(page)(subPrice)
    } yield price

    films map { price => assert(price.isInstanceOf[Purchase]) }

  }

  it should "not get price (watch by sub)" in {

    val films: Future[Price] = for {
      subUrl <- client.subscriptionUrl
      subPage <- client.getContent(subUrl)
      subPrice <- parser.getSubPrice(subPage)
      priceUrl <- client.priceUrl(idSubFilm)
      page <- client.getContent(priceUrl)
      price <- parser.getPrice(page)(subPrice)
    } yield price

    films map { price => assert(price.isInstanceOf[Subscription]) }
  }

}
