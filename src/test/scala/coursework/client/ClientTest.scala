package coursework.client

import coursework.exceptions.UrlNotFoundException
import org.scalatest.flatspec.AsyncFlatSpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class ClientTest extends AsyncFlatSpec {

  val client = new ClientImpl

  behavior of "getContent"

  it should "get content" in {

    assert(Await.result(client.getContent("https://www.ivi.ru/"), Duration.Zero).isInstanceOf[String])

  }

  it should "not get content" in {

    assertThrows[UrlNotFoundException] {
      Await.result(client.getContent("asdafa"), Duration.Zero)
    }

  }

}
