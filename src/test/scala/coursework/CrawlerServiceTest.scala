package coursework

import coursework.exceptions.FilmNotFoundException
import coursework.storage.DataBase
import io.circe.Json
import org.scalatest.flatspec.AsyncFlatSpec

import scala.concurrent.Future

class CrawlerServiceTest extends AsyncFlatSpec {

  implicit val dataBase: DataBase[Future] = DataBase.apply("h2")

  dataBase.createTable

  behavior of "result"

  it should "return json" in {

    val crawlerService = CrawlerService("человек")

    crawlerService.result.map(json => assert(json.isInstanceOf[Json]))
  }

  it should "throw exception" in {

    recoverToSucceededIf[FilmNotFoundException] {
      CrawlerService("asfafsaf").result
    }
  }
}
