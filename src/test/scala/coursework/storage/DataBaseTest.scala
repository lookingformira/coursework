package coursework.storage

import coursework.data.FilmToDB
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future

class DataBaseTest extends AsyncFlatSpec with Matchers {

  implicit val dataBase: DataBase[Future] = DataBase.apply("h2")

  behavior of "createTable"

  it should "create table" in {

    dataBase.createTable

    val future = dataBase.find("asd")

    future.map(films => assert(films.isEmpty))

  }

  behavior of "find"

  it should "find films" in {
    val firstFilm = FilmToDB("first", "firstLink", "price")
    val secondFilm = FilmToDB("second", "secondLink", "price")

    val future = for {
      _ <- dataBase.createTable
      _ <- dataBase.insert(Seq(firstFilm, secondFilm))
      films <- dataBase.find("second")

    } yield films


    future.map(films => films shouldBe  Vector(FilmToDB("second", "secondLink", "price", Some(2))))

  }

  behavior of "insert"

  it should "insert films" in {
    val firstFilm = FilmToDB("film", "firstLink", "price")
    val secondFilm = FilmToDB("film", "secondLink", "price")

    val future = for {
      _ <- dataBase.createTable
      _ <- dataBase.insert(Seq(firstFilm, secondFilm))
      films <- dataBase.find("film")

    } yield films


    future.map(films => assert(films.size == 2))

  }

}
