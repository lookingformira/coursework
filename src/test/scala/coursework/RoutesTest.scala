package coursework

import cats.effect.unsafe.IORuntime
import cats.effect._
import coursework.storage.DataBase
import io.circe._
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import org.scalatest.funspec.AnyFunSpec

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class RoutesTest extends AnyFunSpec {

  implicit val dataBase: DataBase[Future] = DataBase.apply("h2")

  dataBase.createTable

  val route = new Routes()

  describe("GET http://localhost:8080/film/man") {
    it("возвращает json с найденными фильмами") {

      val response: IO[Response[IO]] = route.allRoutes.run(
        Request(Method.GET, uri = uri"/film/man")
      )

      check[Json](response, Status.Ok)
    }
  }

  describe("GET http://localhost:8080/film/asdf") {
    it("возвращает сообщение, что фильм не найден") {

      val response: IO[Response[IO]] = route.allRoutes.run(
        Request(Method.GET, uri = uri"/film/asdf")
      )

      check[String](response, Status.Ok, Some("asdf is not found"))

    }
  }


  implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global

  def check[A](actual: IO[Response[IO]],
               expectedStatus: Status,
               expectedBody: Option[A])(
                implicit ev: EntityDecoder[IO, A]): Boolean = {
    val actualResp = actual.unsafeRunSync()
    val statusCheck = actualResp.status == expectedStatus
    val bodyCheck = expectedBody.fold[Boolean](
      actualResp.body.compile.toVector.unsafeRunSync().isEmpty)(
      expected => actualResp.as[A].unsafeRunSync() == expected
    )
    statusCheck && bodyCheck
  }

  def check[A](actual: IO[Response[IO]],
               expectedStatus: Status)(implicit ev: EntityDecoder[IO, A]): Boolean = {
    val actualResp = actual.unsafeRunSync()
    val statusCheck = actualResp.status == expectedStatus
    val bodyCheck = actualResp.as[A].unsafeRunSync().isInstanceOf[A]
    statusCheck && bodyCheck
  }


}
