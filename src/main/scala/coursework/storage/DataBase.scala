package coursework.storage

import coursework.data.{FilmToDB, FilmsTable}

import scala.concurrent.Future
//import slick.jdbc.H2Profile.api._
import slick.jdbc.PostgresProfile.api._

trait DataBase[F[_]] {

  /**
   * Создание таблицы в базе данных
   */
  def createTable: F[Unit]

  /**
   * Поиск фильмов по базе данных
   *
   * @param title название фильма
   * @return найденные фильмы
   */
  def find(title: String): F[Seq[FilmToDB]]

  /**
   * Добавление фильмов в базу данных
   *
   * @param films коллекция фильмов для добавления в БД
   * @return найденные фильмы
   */
  def insert(films: Seq[FilmToDB]): F[_]

}

object DataBase {
  def apply(config: String): DataBase[Future] = new DataBase[Future] {

    val db = Database.forConfig(config)

    val films: TableQuery[FilmsTable] = TableQuery[FilmsTable]

    override def createTable: Future[Unit] = db.run(films.schema.createIfNotExists)


    override def find(title: String): Future[Seq[FilmToDB]] = {
      val query: Query[FilmsTable, FilmToDB, Seq] = for {
        film <- films if (film.title.toLowerCase like s"$title %") ||
          (film.title.toLowerCase like s"% $title") ||
          (film.title.toLowerCase like s"$title") ||
          (film.title.toLowerCase like s"% $title %")
      } yield film
      db.run(query.result)
    }

    override def insert(addFilms: Seq[FilmToDB]): Future[_] = db.run(films ++= addFilms)
  }
}
