package coursework

import cats.effect._
import com.comcast.ip4s._
import coursework.storage.DataBase
import org.http4s.ember.server._

import scala.concurrent.Future

object MainHttp extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    val route = new Routes()(runtime.compute)
    implicit val dataBase: DataBase[Future] = DataBase.apply("postgres")
    dataBase.createTable
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .withHttpApp(route.allRoutes)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }

}


