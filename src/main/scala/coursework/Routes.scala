package coursework

import cats.data.Kleisli
import cats.effect._
import coursework.storage.DataBase
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.{HttpRoutes, Request, Response}

import scala.concurrent.{ExecutionContext, Future}

class Routes(implicit ec: ExecutionContext) {
  def allRoutes(implicit dataBase: DataBase[Future]): Kleisli[IO, Request[IO], Response[IO]] = HttpRoutes.of[IO] {
    case GET -> Root / "film" / title =>
      Ok(IO.fromFuture(IO(CrawlerService(title.replace("%20", " ")).result)))
        .handleErrorWith(e => BadRequest(e.getMessage))
    case GET -> Root / "hello" / name =>
      Ok(s"hello, $name")
  }.orNotFound

}
