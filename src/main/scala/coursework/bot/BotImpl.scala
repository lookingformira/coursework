package coursework.bot

import cats.effect.{Async, Temporal}
import cats.syntax.functor._
import com.bot4s.telegram.api.declarative.{Callbacks, Commands, Messages}
import com.bot4s.telegram.cats.Polling
import coursework.CrawlerService
import coursework.data.FilmToJson
import coursework.exceptions.FilmNotFoundException
import coursework.storage.DataBase

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class BotImpl[F[_] : Async : Temporal](token: String)(implicit executionContext: ExecutionContext)
  extends Bot[F](token)
    with Polling[F]
    with Commands[F]
    with Messages[F] {

  implicit val dataBase: DataBase[Future] = DataBase.apply("postgres")

  onMessage { implicit msg =>

    val films: Future[Seq[FilmToJson]] = msg.text match {
      case Some(value) => CrawlerService(value).seqRes
      case None => Future.failed(FilmNotFoundException("film"))
    }
    //поправить
    val res = Await.result(films, Duration.Inf)

    reply(s"$res").void
  }
}
