package coursework.data

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class FilmToJson(title: String, link: String, payInfo: String) {
  override def toString: String = s"$title, $link , $payInfo "
}

object FilmToJson {
  implicit val filmToJsonEncoder: Encoder[FilmToJson] = deriveEncoder[FilmToJson]
}
