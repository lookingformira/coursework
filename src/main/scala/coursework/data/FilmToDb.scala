package coursework.data

import slick.jdbc.PostgresProfile.api._
//import slick.jdbc.H2Profile.api._

case class FilmToDB(title: String, link: String, price: String, id: Option[Int] = Some(0))

class FilmsTable(tag: Tag) extends Table[FilmToDB](tag, "out") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def title = column[String]("title")

  def link = column[String]("link")

  def price = column[String]("price")

  def * = (title, link, price, id.?) <> (FilmToDB.tupled, FilmToDB.unapply)
}


