package coursework.data

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class FilmFromIvi(id: Int, title: String)

object FilmFromIvi {
  implicit val filmDecoder: Decoder[FilmFromIvi] = deriveDecoder[FilmFromIvi]
}
