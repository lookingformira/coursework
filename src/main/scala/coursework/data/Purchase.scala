package coursework.data

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

trait Price

case class Subscription(price: String) extends Price {
  override def toString: String = s"Подписка: $price"
}

case class Purchase(min: String, max: String) extends Price {
  override def toString: String = (min, max) match {
    case (lower, upper) if lower == upper => s"цена покупки $lower рублей"
    case _ => s"цена покупки от $min до $max рублей"
  }
}

object Purchase {
  implicit val filmDecoder: Decoder[Purchase] = deriveDecoder[Purchase]
}

case class SubPrice(price: String, object_title: String, product_title: String)

object SubPrice {
  implicit val subDecoder: Decoder[SubPrice] = deriveDecoder[SubPrice]
}
