package coursework.data

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class Token(value: String)

object Token{
  implicit val tokenDecoder: Decoder[Token] = deriveDecoder[Token]
}
