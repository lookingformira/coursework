package coursework

import cats.effect.{ExitCode, IO, IOApp}
import coursework.bot.BotImpl
import coursework.client.ClientImpl
import coursework.parser.ParserImpl
import coursework.storage.DataBase

import scala.concurrent.{ExecutionContext, Future}

object MainBot extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val ec: ExecutionContext = runtime.compute
    val client = new ClientImpl
    val parser = new ParserImpl
    implicit val dataBase: DataBase[Future] = DataBase.apply("postgres")
    dataBase.createTable
    for {
      json <- IO.fromFuture(IO(client.getToken("src/main/resources/token.json")))
      token <- IO.fromFuture(IO(parser.getTokenContent(json)))
      bot: BotImpl[IO] = new BotImpl[IO](token.value)
      _ <- bot.run()
    } yield ExitCode.Success
  }
}
