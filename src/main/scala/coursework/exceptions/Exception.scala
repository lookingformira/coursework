package coursework.exceptions

sealed abstract class CrawlerException(message: String)
  extends Exception(message)

final case class UrlNotFoundException(url: String)
  extends CrawlerException(s"$url is not found")

final case class FilmNotFoundException(title: String)
  extends CrawlerException(s"$title is not found")

final case class TokenNotFoundException(path: String)
  extends CrawlerException(s"token in $path is not found")
