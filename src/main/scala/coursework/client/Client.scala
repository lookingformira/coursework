package coursework.client

import scala.concurrent.Future

trait Client {

  /**
   * Переходит по url-адресу и возвращает json
   *
   * @param url адрес
   * @return json-файл
   */
  def getContent(url: String): Future[String]

}
