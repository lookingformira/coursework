package coursework.client

import coursework.exceptions.{TokenNotFoundException, UrlNotFoundException}

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import scala.util.{Failure, Success, Try}

class ClientImpl(implicit ec: ExecutionContext) extends Client {

  def subscriptionUrl: Future[String] = Future
    .successful("https://api2.ivi.ru/mobileapi/billing/v1/purchase/options/?app_version=870&fields=price_ranges%2Cpurchases%2Cpurchase_options&session=ec40447b1730448691_1665342877-535394811n0aPLrc2ndMNDUhfMY0f4g&user_ab_bucket=7129&with_affiliate_subscriptions=1&with_bundle_subscriptions=1&with_long_subscriptions=1&with_partial_debit=1&with_subscription_renewals=1")

  /**
   * Преобразует название фильма в строку для запроса
   *
   * @param filmName название фильма
   * @return строка для запроса
   */
  def filmUrl(filmName: String, page: Int): Future[String] = {
    val from = (page - 1) * 100
    val to = page * 100 - 1
    Future
      .successful(s"https://api2.ivi.ru/mobileapi/search/v7/?query=${filmName.replace(" ", "+")}&from=$from&to=$to&user_ab_bucket=7129&fields=id,title,year&app_version=870")

  }

  /**
   * Преобразует id фильма в строку для запроса
   *
   * @param filmId фильма
   * @return строка для запроса
   */
  def priceUrl(filmId: Int): Future[String] = Future
    .successful(s"https://api2.ivi.ru/mobileapi/billing/v1/purchase/content/options/?id=$filmId&with_partial_debit=1&with_affiliate_subscriptions=1&with_bundle_subscriptions=1&with_trial=1&user_ab_bucket=7129&app_version=870&session=ec40447b1730448691_1665342877-535394811n0aPLrc2ndMNDUhfMY0f4g")

  /**
   * Переходит по url-адресу и возвращает json
   *
   * @param url адрес
   * @return json-файл
   */
  override def getContent(url: String): Future[String] = Try {
    Source
      .fromURL(url)
  } match {
    case Success(value) => Future.successful(value.mkString)
    case Failure(_) => Future.failed(UrlNotFoundException(url))
  }

  def getToken(path: String): Future[String] = Try {
    Source.fromFile(path)
  } match {
    case Success(value) => Future.successful(value.mkString)
    case Failure(_) => Future.failed(TokenNotFoundException(path))
  }

}


