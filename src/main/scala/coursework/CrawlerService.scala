package coursework

import coursework.client.ClientImpl
import coursework.data.{FilmFromIvi, FilmToDB, FilmToJson}
import coursework.exceptions.FilmNotFoundException
import coursework.parser.ParserImpl
import coursework.storage.DataBase
import io.circe.Json
import io.circe.syntax._

import scala.concurrent.{ExecutionContext, Future}

case class CrawlerService(filmName: String)(implicit ec: ExecutionContext) {

  val client = new ClientImpl

  val parser = new ParserImpl

  def result(implicit dataBase: DataBase[Future]): Future[Json] =
    seqRes.map(_.asJson)

  def seqRes(implicit dataBase: DataBase[Future]): Future[Seq[FilmToJson]] = {
    dataBase.find(filmName).flatMap {
      case seq if seq.isEmpty => for {
        subUrl <- client.subscriptionUrl
        subPage <- client.getContent(subUrl)
        subPrice <- parser.getSubPrice(subPage)
        films <- getAllFilms(getHundredFilms())
        pricesUrlFut: Future[Seq[String]] = Future.sequence(films.map(film => client.priceUrl(film.id)))
        pricesUrl <- pricesUrlFut
        pagePriceFut = Future.sequence(pricesUrl.map(url => client.getContent(url)))
        pagePrice <- pagePriceFut
        pricesFut = Future.sequence(pagePrice.map(pricePage => parser.getPrice(pricePage)(subPrice)))
        prices <- pricesFut
        filmsPrices = films zip prices
        filmsToJson = filmsPrices.map {
          case (film, price) => FilmToJson(film.title, s"https://www.ivi.ru/watch/${film.id}", price.toString)
        }
        json = filmsToJson
        filmsToDb = filmsPrices.map {
          case (film, price) => FilmToDB(film.title, s"https://www.ivi.ru/watch/${film.id}", price.toString)
        }
        _ <- dataBase.insert(filmsToDb)
      } yield json
      case seq => Future(seq.map(film => FilmToJson(film.title, film.link, film.price)))
    }
  }

  private def getHundredFilms(page: Int = 1): Future[Seq[FilmFromIvi]] = for {
    filmUrl <- client.filmUrl(filmName, page)
    site <- client.getContent(filmUrl)
    films <- parser.getFilm(site)
  } yield films


  private def getAllFilms(end: Future[Seq[FilmFromIvi]]): Future[Seq[FilmFromIvi]] = {
    def loop(trig: Future[Seq[FilmFromIvi]],
             acc: Future[Seq[FilmFromIvi]] = Future.successful(Seq.empty[FilmFromIvi]),
             page: Int = 1): Future[Seq[FilmFromIvi]] = trig.flatMap {
      case seq if seq.isEmpty => acc
      case _ => loop(getHundredFilms(page + 1), getHundredFilms(page).flatMap(seq => acc.map(_ ++ seq)), page + 1)
    }

    loop(end).flatMap {
      case seq if seq.isEmpty => Future.failed(FilmNotFoundException(filmName))
      case seq => Future.successful(seq)
    }
  }
}
