package coursework.parser

import coursework.exceptions.{FilmNotFoundException, TokenNotFoundException, UrlNotFoundException}
import coursework.data.{FilmFromIvi, Price, Purchase, SubPrice, Subscription, Token}
import io.circe.Decoder
import io.circe.parser._

import scala.concurrent.{ExecutionContext, Future}

class ParserImpl(implicit executionContext: ExecutionContext) extends Parser {

  def getTokenContent(json: String): Future[Token] = {
    val decodeParam = Decoder[Token]
    decode(json)(decodeParam) match {
      case Left(_) => Future.failed(TokenNotFoundException(json))
      case Right(value) => Future.successful(value)
    }
  }

  /**
   * На заданной странице находит соответствующие названию фильмы
   *
   * @param json для парсинга
   * @return найденные фильмы
   */
  override def getFilm(json: String): Future[Seq[FilmFromIvi]] = {
    val decodeParam = Decoder[Seq[FilmFromIvi]].prepare(_.downField("result"))
    decode(json)(decodeParam) match {
      case Left(_) => Future.failed(FilmNotFoundException("film is not found"))
      case Right(seq) => Future.successful(seq)
    }
  }

  /**
   * На заданной странице находит условия для просмотра
   *
   * @param json     для парсинга
   * @param subPrice цена подписки
   * @return условия для просмотра
   */
  override def getPrice(json: String)(subPrice: String): Future[Price] = {
    val decodeParam = Decoder[Purchase].prepare(_.downField("result").downField("price_ranges").downField("price"))
    decode(json)(decodeParam) match {
      case Left(_) => Future.successful(Subscription(subPrice))
      case Right(price) => Future.successful(price)
    }
  }

  /**
   * На заданной странице находит цену подписки
   *
   * @param json для парсинга
   * @return цена подписки
   */
  def getSubPrice(json: String): Future[String] = {
    val decodeParam = Decoder[Seq[SubPrice]].prepare(_.downField("result").downField("purchase_options"))
    val titleRegExp = "(.*)\\s+([1-9]{1,2})\\s+(мес.*|лет.*|год.*)".r
    decode(json)(decodeParam) match {
      case Left(_) => Future.failed(UrlNotFoundException(json))
      case Right(price) => Future.successful {
        price.filter(_.object_title == "ivi").map {
          case SubPrice(price, _, title) =>
            title match {
              case titleRegExp(_, num, _) => s"$num мес - $price рублей"
              case _ => "Ошибка сопоставления строки"
            }
        }.mkString(", ")
      }
    }
  }

}
