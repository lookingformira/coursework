package coursework.parser

import coursework.data.{FilmFromIvi, Price}

import scala.concurrent.Future

trait Parser {
  /**
   * На заданной странице находит соответствующие названию фильмы
   *
   * @param json для парсинга
   * @return найденные фильмы
   */
  def getFilm(json: String): Future[Seq[FilmFromIvi]]

  /**
   * На заданной странице находит условия для просмотра
   *
   * @param json     для парсинга
   * @param subPrice цена подписки
   * @return условия для просмотра
   */
  def getPrice(json: String)(subPrice: String): Future[Price]
}
