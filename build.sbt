name := "coursework"

version := "0.1"

scalaVersion := "2.13.8"

val http4sVersion = "1.0.0-M23"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-ember-server" % http4sVersion,
  "org.http4s" %% "http4s-ember-client" % http4sVersion,

  "com.typesafe.slick" %% "slick" % "3.3.3",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
  "org.postgresql" % "postgresql" % "42.3.5",
  "com.h2database" % "h2" % "1.4.200",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "org.scalatest" %% "scalatest" % "3.2.4" % Test,

  "com.bot4s" %% "telegram-core" % "5.4.2",
  "com.bot4s" %% "telegram-akka" % "5.4.2",

  "org.typelevel" %% "cats-effect" % "3.3.9",

  "com.softwaremill.sttp.client3" %% "core" % "3.6.2",
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-cats" % "3.6.2",
)
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % "0.14.1")
